package akka.first.app;

import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.first.app.actors.CrawlerManager;
import akka.first.app.messages.CrawlerJobType;
import akka.first.app.messages.GeneralJobMessage;
import junit.framework.TestCase;

/**
 * @author Zoran Jeremic 2013-09-30
 */
public class MainClassTest extends TestCase {

	public void testMain() {
		ActorSystem _system = ActorSystem.create("crawlerActorsSystem",ConfigFactory.load().getConfig("InextwebCrawler"));
		ActorRef crawlerActor= _system.actorOf(Props.create(CrawlerManager.class, "param1","param2")
	     		 .withDispatcher("priorDispatcher"));
		crawlerActor.tell(new String("start"), null);
		try {
			Thread.sleep(1000000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
