package akka.first.app.actors;


 
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

import java.util.LinkedList;

 

import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.first.app.messages.CrawlerJobType;
import akka.first.app.messages.GeneralJobMessage;

/**
 * @author Zoran Jeremic 2013-07-15
 */
public class CrawlerManager  extends UntypedActor{
	ActorRef crawlerActor;
	public CrawlerManager(String param1, String param2){
		 crawlerActor= getContext().actorOf(Props.create(DefaultCrawlerActor.class, this.getSelf(),
				 "param1","param2")
	     		 .withDispatcher("priorDispatcher"));
	}
	@Override
	public void onReceive(Object message) throws Exception {
		if(message instanceof GeneralJobMessage){
		if(((GeneralJobMessage) message).getJobType().equals(CrawlerJobType.OTHERJOB)){
				System.out.println("EXECUTED OTHERJOB with ID:"+((GeneralJobMessage) message).getCounter());
			}
	}
		else if(message instanceof String){
			startJobs();
		}
	}
	
	private void startJobs(){
		int counter=0;
		while(counter<1000){
			counter++;
				 SecureRandom random = new SecureRandom();
            	 String id= new BigInteger(130, random).toString(32);
            	System.out.println("Crawler Manager Created other job with id:"+id);
				crawlerActor.tell(new GeneralJobMessage(CrawlerJobType.OTHERJOB, id), null);
		}
	}
	 
	
}
