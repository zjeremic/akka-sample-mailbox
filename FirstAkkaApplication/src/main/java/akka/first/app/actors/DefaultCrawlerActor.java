package akka.first.app.actors;

 

 

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.LinkedList;
import java.util.List;

 

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.first.app.messages.CrawlerJobType;
import akka.first.app.messages.GeneralJobMessage;

/**
 * @author Zoran Jeremic 2013-07-15
 */
public class DefaultCrawlerActor  extends UntypedActor {
	public boolean started;
	//private List<ActorRef> workers= new LinkedList<ActorRef>();
	ActorRef _parent;
//ActorRef worker;
	public DefaultCrawlerActor(ActorRef parent, String param1, String param2){
		_parent=parent;
		
	}
	@Override
	public void onReceive(Object message) throws Exception {
		if(message instanceof GeneralJobMessage){
			 if(((GeneralJobMessage) message).getJobType().equals(CrawlerJobType.OTHERJOB)){
					System.out.println("PROCESSED OTHER JOB:"+((GeneralJobMessage) message).getCounter());

			}
		}
	}
}
