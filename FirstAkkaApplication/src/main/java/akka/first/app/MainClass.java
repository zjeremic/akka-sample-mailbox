package akka.first.app;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.first.app.actors.CrawlerManager;

import com.typesafe.config.ConfigFactory;

/**
 * @author Zoran Jeremic 2013-07-15
 */
public class MainClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	 
		
		ActorSystem _system = ActorSystem.create("crawlerActorsSystem",ConfigFactory.load().getConfig("InextwebCrawler"));
		ActorRef crawlerActor= _system.actorOf(Props.create(CrawlerManager.class, "param1","param2")
	     		 .withDispatcher("priorDispatcher"));
		crawlerActor.tell(new String("start"), null);
	}

}
