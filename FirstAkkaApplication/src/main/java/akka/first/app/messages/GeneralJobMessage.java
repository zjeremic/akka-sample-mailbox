package akka.first.app.messages;
/**
 * @author Zoran Jeremic 2013-07-15
 */
public class GeneralJobMessage {
	private CrawlerJobType jobType;
	private String counter;
	
	public GeneralJobMessage(CrawlerJobType type, String count){
		jobType=type;
		counter=count;
	}

	public CrawlerJobType getJobType() {
		// TODO Auto-generated method stub
		return jobType;
	}
	public String getCounter(){
		return counter;
	}

}
