package akka.first.app;

 
import com.typesafe.config.Config;
import akka.actor.ActorSystem;
import akka.dispatch.PriorityGenerator;
import akka.dispatch.UnboundedPriorityMailbox;
import akka.first.app.messages.CrawlerJobType;
import akka.first.app.messages.GeneralJobMessage;

/**
 * @author Zoran Jeremic 2013-07-10
 */
public class CrawlerTasksMailbox extends UnboundedPriorityMailbox {

	public CrawlerTasksMailbox(ActorSystem.Settings settings, Config config) {
		super(new PriorityGenerator(){
			@Override
			public int gen(Object message) {
				if(message instanceof GeneralJobMessage){
					  if(((GeneralJobMessage) message).getJobType().equals(CrawlerJobType.OTHERJOB)){
						System.out.println("MAILBOX RECEIVED GENERAL OTHER JOB MESSAGE:"+((GeneralJobMessage) message).getCounter());
						return 0;
					}else{
						return 2;
					}
				}else  
					return 1;
			 
			}
			});
		}
}
